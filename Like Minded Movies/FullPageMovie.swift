//
//  FullPageMovie.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 21/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit

struct FullPageMovie: MovieProtocol, FullMovieProtocol {
    var title: String? = ""
    var imdbId: String? = ""
    var image: String? = ""
    var trailer: String? = ""
    var runtime: String? = ""
    var overview: String? = ""
    var year: String? = ""
    var certification: String? = ""
    var genres: String? = ""
    var rating: String? = ""
    
    
    mutating func configureForDictionary(_ dictionary: NSDictionary){
        
        if (dictionary["title"] as? String) != nil {
            self.title = dictionary["title"] as? String
        }
        
        self.runtime = NSString(format: "Runtime: %@ mins", ((dictionary["runtime"] as AnyObject).stringValue!)) as String
        
        if (dictionary["overview"] as? String) != nil {
            self.overview = dictionary["overview"] as? String
        }

        self.year = NSString(format: "Year: %@", ((dictionary["year"] as AnyObject).stringValue!)) as String
        
        if (dictionary["certification"] as? String) != nil {
            self.certification = NSString(format: "Certification: %@", dictionary["certification"] as! String) as String
        }
        
        self.imdbId = dictionary["ids"]!["imdb"] as? String
        
        let ratingNumber = dictionary["rating"] as! NSNumber
        self.rating = NSString(format: "Rating: %.1f/10", ratingNumber.floatValue) as String
        
        var trailerUrl = ""
        
        let url = dictionary["trailer"]
        
        if url is NSNull{
        }
        else{
            trailerUrl = (url as? String)!
        }
        
        self.trailer = trailerUrl
        
        let imagesDict = dictionary["images"]!["poster"]
        self.image = imagesDict!!["medium"] as? String
        
        if (dictionary["genres"] as? NSArray) != nil {
            let genres = dictionary["genres"] as! NSArray
            let genreString = NSMutableString()
            
            for genre in genres{
                genreString.append(genre as! String)
                
                if (!(genre as AnyObject).isEqual(genres.lastObject)){
                    genreString.append(", ")
                }
            }
            
            self.genres = NSString(format: "Genre: %@", genreString) as String
        }
    }
}
