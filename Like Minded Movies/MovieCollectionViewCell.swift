//
//  MovieCollectionViewCell.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 14/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.masksToBounds = false
        self.clipsToBounds = false
        self.layer.shadowColor = UIColor.white.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 5
    }
    
    func configureForMovie(_ movie: TraktMovie){
        
        self.titleLabel.text = movie.title
        
        if(movie.imageUrl != nil){
            self.imageView.sd_setImage(with: URL(string: movie.imageUrl!), placeholderImage: UIImage(named:"Placeholder"))
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.titleLabel.text = nil
        self.imageView.image = nil
        self.imageView.stopDownload()
    }
    
}
