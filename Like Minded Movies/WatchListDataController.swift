//
//  WatchListDataController.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 21/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit
import CoreData
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class WatchListDataController {
    static let sharedInstance = WatchListDataController()
    var managedContext: NSManagedObjectContext!
    
    fileprivate init() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.managedContext = appDelegate.managedObjectContext
    }
    
    func getWatchListItems(_ completion: (_ error: NSError?, _ watchList: [FullMovie]) -> Void){
        
        var watchListMovies = [FullMovie]()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FullMovie")
        
        var returnError: NSError?
        
        do {
            let results =
            try managedContext!.fetch(fetchRequest)
            watchListMovies = results as! [FullMovie]
        } catch let error as NSError {
            returnError = error
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        completion(returnError, watchListMovies)
    }
    
    func isMovieItemOnWatchList(_ imdbId: String) -> Bool{
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FullMovie")
        fetchRequest.predicate = NSPredicate(format:"imdbId == %@", imdbId)
        
        var found = false
        
        do {
            let results =
                try managedContext!.fetch(fetchRequest)
            found = (results.count > 0)
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        return found
    }
        
    func saveCast(_ cast: [CastModel], fullMovie: FullMovie, completion: (_ error: NSError?) -> Void){
        
        let entity =  NSEntityDescription.entity(forEntityName: "Cast",
            in:self.managedContext!)
        
        let mutableItems = fullMovie.cast!.mutableCopy() as! NSMutableOrderedSet
        
        for castModel in cast{
            
            let castObject = NSManagedObject(entity: entity!,
                insertInto:self.managedContext) as! Cast
            
            castObject.setValue(castModel.character, forKey: "character")
            castObject.setValue(castModel.actor, forKey: "actor")
            castObject.setValue(castModel.movieId, forKey: "movieId")
        
            mutableItems.add(castObject)
        }
        
        fullMovie.cast = mutableItems.copy() as? NSOrderedSet
        
        var returnError: NSError?
        
        do {
            try managedContext!.save()
            
        } catch let error as NSError {
            returnError = error
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        completion(returnError)
    }
    
    func saveWatchListItem(_ fullPageMovie: TraktMovie, completion: (_ error: NSError?, _ fullMovie: FullMovie?) -> Void) {

        let entity =  NSEntityDescription.entity(forEntityName: "FullMovie",
            in:self.managedContext!)
        
        let fullMovie = NSManagedObject(entity: entity!,
            insertInto:self.managedContext) as! FullMovie
        
        fullMovie.setValue(fullPageMovie.title, forKey: "title")
        fullMovie.setValue(fullPageMovie.overview, forKey: "overview")
        fullMovie.setValue(fullPageMovie.trailer?.absoluteString, forKey: "trailerUrl")
        fullMovie.setValue(fullPageMovie.imageUrl, forKey: "imageUrl")
        fullMovie.setValue(fullPageMovie.genreString, forKey: "genres")
        fullMovie.setValue(fullPageMovie.certification, forKey: "certification")
        fullMovie.setValue(fullPageMovie.year, forKey: "year")
        fullMovie.setValue(fullPageMovie.runtime, forKey: "runtime")
        fullMovie.setValue(fullPageMovie.ids.imdb, forKey: "imdbId")
        
        var returnError: NSError?
        
        do {
            try managedContext!.save()
            
        } catch let error as NSError {
            returnError = error
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        completion(returnError, fullMovie)
    }
    
    func deleteFullPageMovie(_ fullPageMovie: TraktMovie){
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FullMovie")
        fetchRequest.predicate = NSPredicate(format:"imdbId == %@", fullPageMovie.ids.imdb!)
        
        do {
            let results = try managedContext!.fetch(fetchRequest) as! [FullMovie]
            
            for movies in results{
                if movies.imdbId == fullPageMovie.ids.imdb {
                    let fullMovie = movies
                    
                    for cast in fullMovie.cast!{
                        self.managedContext.delete(cast as! NSManagedObject)
                    }
                    
                    self.managedContext.delete(fullMovie as NSManagedObject)
                    break
                }
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }

        do {
            try managedContext!.save()
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func deleteFullMovie(_ fullMovie: FullMovie){
        
        if fullMovie.cast?.count > 0{
            for cast in fullMovie.cast!{
                self.managedContext.delete(cast as! NSManagedObject)
            }
        }
        
        self.managedContext.delete(fullMovie as NSManagedObject)

        do {
            try managedContext!.save()
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
}
