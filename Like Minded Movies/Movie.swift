//
//  Movie.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 14/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import Foundation

struct Movie: MovieProtocol {
    let title: String?
    let imdbId: String?
    let image: String?
}