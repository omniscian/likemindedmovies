//
//  FullMovie+CoreDataProperties.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 22/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension FullMovie {

    @NSManaged var certification: String?
    @NSManaged var genres: String?
    @NSManaged var imageUrl: String?
    @NSManaged var imdbId: String?
    @NSManaged var overview: String?
    @NSManaged var runtime: String?
    @NSManaged var title: String?
    @NSManaged var trailerUrl: String?
    @NSManaged var year: String?
    @NSManaged var rating: String?
    @NSManaged var cast: NSOrderedSet?

}
