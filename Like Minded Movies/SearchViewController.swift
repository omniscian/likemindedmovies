//
//  ViewController.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 14/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit
import SafariServices

class SearchViewController: UIViewController, MoviesCollectionViewControllerDelegate {

    @IBOutlet var textField: UITextField!
    var searchMoviesArray = Array<MovieProtocol>()
    var relatedMoviesArray = Array<MovieProtocol>()
    var moviesViewController: MoviesCollectionViewController!
    
    var selectedMovie: TraktMovie!
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "EmbedMovieCollectionViewController"){
            self.moviesViewController = segue.destination as? MoviesCollectionViewController
            self.moviesViewController?.delegate = self;
        }
        else if (segue.identifier == "SearchToFullMovieViewController"){
            let fullPageMovieViewController:FullPageMovieViewController = segue.destination as! FullPageMovieViewController
            fullPageMovieViewController.configureForMovieId(_movieId: self.selectedMovie.ids.imdb!, _imageUrl: self.selectedMovie.imageUrl!)
        }
    }

    @IBAction func searchButtonPressed(_ sender: UIButton){

        let searchText = textField.text?.replacingOccurrences(of: " ", with: "-")
        
        self.moviesViewController.requestForMovieType(MovieType.search, searchText:searchText)
        self.textField.resignFirstResponder()
    }
    
    func moviesCollectionViewControllerDidSelectItem(_ movie: TraktMovie, controller: MoviesCollectionViewController){
        
        self.selectedMovie = movie
        self.performSegue(withIdentifier: "SearchToFullMovieViewController", sender: nil)
    }
}

