//
//  AboutViewController.swift
//  Like Minded Movies
//
//  Created by Ian Houghton on 28/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.rightBarButtonItem = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func traktButtonPressed(_ sender: UIButton){
        UIApplication.shared.openURL(URL(string: "http://docs.trakt.apiary.io")!)
    }
    
    @IBAction func yakAppsButtonPressed(_ sender: UIButton){
        UIApplication.shared.openURL(URL(string: "http://yakapps.co.uk")!)
    }
}
