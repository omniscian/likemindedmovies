//
//  HomeViewController.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 14/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit
import GoogleMobileAds

class HomeViewController: UIViewController, MoviesCollectionViewControllerDelegate, GADBannerViewDelegate {
    
    var trendingViewController: MoviesCollectionViewController!
    var popularMoviesViewController: MoviesCollectionViewController!
    var mostPlayedMoviesViewController: MoviesCollectionViewController!
    var anticipatedMoviesViewController: MoviesCollectionViewController!
    
    @IBOutlet private var bannerHolderView: UIView!
    @IBOutlet private var bannerViewHeightConstraint: NSLayoutConstraint!
    
    private var bannerView: GADBannerView?
    
    var selectedMovieId = String()
    var selectedMovieURL = String()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "EmbedTrendingViewControllerToHome"){
            self.trendingViewController = segue.destination as? MoviesCollectionViewController
            self.trendingViewController?.delegate = self
        }
        else if (segue.identifier == "EmbedPopularViewControllerToHome"){
            self.popularMoviesViewController = segue.destination as? MoviesCollectionViewController
            self.popularMoviesViewController?.delegate = self
        }
        else if (segue.identifier == "EmbedMostPlayedViewControllerToHome"){
            self.mostPlayedMoviesViewController = segue.destination as? MoviesCollectionViewController
            self.mostPlayedMoviesViewController?.delegate = self
        }
        else if (segue.identifier == "EmbedMostAnticipatedViewControllerToHome"){
            self.anticipatedMoviesViewController = segue.destination as? MoviesCollectionViewController
            self.anticipatedMoviesViewController?.delegate = self
        }
        else if (segue.identifier == "HomeToFullPageMovieViewController"){
            
            let fullPageMovieViewController = segue.destination as! FullPageMovieViewController
            
            fullPageMovieViewController.configureForMovieId(_movieId: selectedMovieId, _imageUrl: selectedMovieURL)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.trendingViewController.requestForMovieType(MovieType.trending, searchText: nil)
        self.popularMoviesViewController.requestForMovieType(MovieType.popular, searchText: nil)
        self.anticipatedMoviesViewController.requestForMovieType(MovieType.anticipated, searchText: nil)
        
        self.bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        self.bannerView?.delegate = self
        self.bannerHolderView.addSubview(self.bannerView!)
        self.bannerView?.adUnitID = "ca-app-pub-9284667006743183/2527243301"
        self.bannerView?.rootViewController = self
        
        let request = GADRequest()
        request.testDevices = [ kGADSimulatorID];
        
        self.bannerView?.load(request)
    }
    
    func moviesCollectionViewControllerDidSelectItem(_ movie: TraktMovie, controller: MoviesCollectionViewController){
        
        self.selectedMovieId = movie.ids.imdb!
        self.selectedMovieURL = movie.imageUrl!
        
        self.performSegue(withIdentifier: "HomeToFullPageMovieViewController", sender: nil)
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        UIView.animate(withDuration: 1, animations: {
            self.bannerViewHeightConstraint.constant = 50
        })
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print(error)
    }
}
