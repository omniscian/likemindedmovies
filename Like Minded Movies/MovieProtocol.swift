//
//  MovieProtocol.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 14/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import Foundation

protocol MovieProtocol {
    var title: String? { get }
    var imdbId: String? { get }
    var image: String? { get }
}

protocol FullMovieProtocol {
    var trailer: String? { get }
    var runtime: String? { get }
    var overview: String? { get }
    var year: String? { get }
    var certification: String? { get }
    var genres: String? { get }
    var rating: String? { get }
}

protocol CastProtocol {
    var actor: String? { get }
    var character: String? { get }
    var movieId: String? { get }
}