//
//  UIImageView+Download.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 14/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit
import ObjectiveC

private var url: String = ""

extension UIImageView {
    
    func downloadedFrom(link:String, contentMode mode: UIView.ContentMode, completion: @escaping (_ completed: Bool) -> Void) {
        
        url = link
        guard
            let url = URL(string: link)
            else {return}
        contentMode = mode
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async { () -> Void in
                completion(true)
                self.image = image
            }
        }).resume()
    }
    
    func stopDownload(){
        URLSession.shared.getAllTasks { (tasks: [URLSessionTask]) -> Void in
            for task in tasks {
                if (task.currentRequest?.url?.absoluteString == url){
                    task.cancel()
                }
            }
        }
    }
}
