//
//  MenuTableViewCell.swift
//  Like Minded Movies
//
//  Created by Ian Houghton on 13/11/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var movieImageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.titleLabel.text = nil
        self.movieImageView.image = nil
        self.movieImageView.stopDownload()
    }
}
