//
//  ShowSimilarViewController.swift
//  Like Minded Movies
//
//  Created by Ian Houghton on 29/05/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit

class ShowSimilarViewController: UIViewController, MoviesCollectionViewControllerDelegate {

    @IBOutlet var posterImageView: UIImageView!
    var similarViewController: MoviesCollectionViewController?
    var selectedMovie: TraktMovie!
    var fullPageMovie: TraktMovie!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.posterImageView.sd_setImage(with: URL(string: self.fullPageMovie.imageUrl!))
        self.posterImageView.layer.masksToBounds = false
        self.posterImageView.clipsToBounds = false
        self.posterImageView.layer.shadowColor = UIColor.white.cgColor
        self.posterImageView.layer.shadowOpacity = 0.5
        self.posterImageView.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.posterImageView.layer.shadowRadius = 5
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "EmbedSimilarViewController") {
            self.similarViewController = segue.destination as? MoviesCollectionViewController
            self.similarViewController?.requestForMovieType(.related, searchText: self.fullPageMovie.ids.imdb);
            self.similarViewController?.delegate = self;
        }
        else if (segue.identifier == "SimilarToFullMovieViewController") {
            let fullPageMovieViewController = segue.destination as! FullPageMovieViewController
            fullPageMovieViewController.configureForMovieId(_movieId: self.selectedMovie.ids.imdb!, _imageUrl: self.selectedMovie.imageUrl!)
        }
    }
    
    func moviesCollectionViewControllerDidSelectItem(_ movie: TraktMovie, controller: MoviesCollectionViewController){
        
        self.selectedMovie = movie
        self.performSegue(withIdentifier: "SimilarToFullMovieViewController", sender: nil)
    }
}
