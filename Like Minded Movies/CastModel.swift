//
//  Cast.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 21/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit

struct CastModel: CastProtocol {
    let actor: String?
    let character: String?
    let movieId: String?
}
