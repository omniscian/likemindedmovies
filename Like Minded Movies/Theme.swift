//
//  Theme.swift
//  Like Minded Movies
//
//  Created by Ian Houghton on 01/06/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit

let SelectedThemeKey = "SelectedTheme"

struct ThemeManager {
    static func applyTheme() {
        // Title text color Black => Text appears in white
        UINavigationBar.appearance().barStyle = UIBarStyle.black
        
        // Translucency; false == opaque
        UINavigationBar.appearance().isTranslucent = false
        
        // BACKGROUND color of nav bar
        UINavigationBar.appearance().barTintColor = UIColor(red: 41.0/255.0, green: 113.0/255.0, blue: 178.0/255.0, alpha: 1.0)
        
        // Foreground color of bar button item text, e.g. "< Back", "Done", and so on.
        UINavigationBar.appearance().tintColor = UIColor.white
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18)]
        
        UITabBar.appearance().barTintColor = UIColor(red: 41.0/255.0, green: 113.0/255.0, blue: 178.0/255.0, alpha: 1.0)
        UITabBar.appearance().tintColor = UIColor.white
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for:.selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightGray], for:.normal)
    }
}
