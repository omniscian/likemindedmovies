//
//  Cast+CoreDataProperties.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 22/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Cast {

    @NSManaged var actor: String?
    @NSManaged var character: String?
    @NSManaged var movieId: String?

}
