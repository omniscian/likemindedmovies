//
//  WatchListViewController.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 21/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit
import CoreData

class WatchListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    var dataSource: [FullMovie] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.getWatchListItems()
    }
    
    func getWatchListItems(){
        WatchListDataController.sharedInstance.getWatchListItems { (error, watchList) in
            guard error == nil else {
                print("error calling GET on /posts/1")
                print(error!)
                return
            }
            
            self.dataSource = watchList
            
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as! MenuTableViewCell
        
        let fullMovie = self.dataSource[indexPath.row] as FullMovie
        
        cell.titleLabel?.text = fullMovie.title
        cell.movieImageView.sd_setImage(with: URL(string: fullMovie.imageUrl!), placeholderImage: UIImage(named:"Placeholder"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let fullMovie = self.dataSource[indexPath.row] as FullMovie
        
        let navController = self.storyboard?.instantiateViewController(withIdentifier: "FullPageMovieNavController") as! UINavigationController
        let fullPageMovieViewController = navController.topViewController as! FullPageMovieViewController
        
        fullPageMovieViewController.configureForMovieId(_movieId: fullMovie.imdbId!, _imageUrl: fullMovie.imageUrl!)
        
        self.present(navController, animated: true) {}
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let fullMovie = self.dataSource[indexPath.row] as FullMovie
            
            WatchListDataController.sharedInstance.deleteFullMovie(fullMovie)
            
            self.getWatchListItems()
        }
    }

}
