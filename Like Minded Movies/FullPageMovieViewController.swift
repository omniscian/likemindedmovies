//
//  FullPageMovieViewController.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 17/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit
import CoreData

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class FullPageMovieViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MoviesCollectionViewControllerDelegate {

    @IBOutlet var posterImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var overviewLabel: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var runtimeLabel: UILabel!
    @IBOutlet var certificationLabel: UILabel!
    @IBOutlet var genreLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var trailerButton: UIButton!
    @IBOutlet var watchListButton: UIButton!
    @IBOutlet var viewSimilarButton: UIButton!
    @IBOutlet var castTableView: UITableView!
    @IBOutlet var castTableViewHeightConstraint: NSLayoutConstraint!
    
    var fullPageMovie: TraktMovie!
    var castArray: [CastModel] = []
    var movieId : String!
    var movieImageUrl : String!
    var movieSavedToWatchList = false
    
    func isModal() -> Bool {
        
        if self.isMovingToParent {
            return false
        }
        
        if self.presentingViewController != nil {
            return true
        }
        
        if self.presentingViewController?.presentedViewController == self {
            return true
        }
        
        if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
            return true
        }
        
        if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        
        return false
    }
    
    override var previewActionItems: [UIPreviewActionItem] {
        
        var showAction = UIPreviewAction()
        var actions = [UIPreviewAction]()
        
        if self.fullPageMovie != nil {
            if self.fullPageMovie.trailer != nil {
                showAction = UIPreviewAction(title: "View Trailer", style: .default) { (action: UIPreviewAction, vc: UIViewController) -> Void in
                    self.viewTrailerButtonPressed(nil)
                }
                
                actions.append(showAction)
            }
        }
        
        
        let saveAction = UIPreviewAction(title: "Save to Watch List", style: .default) { (action: UIPreviewAction, vc: UIViewController) -> Void in
            self.saveToWatchList(nil)
        }
        
        actions.append(saveAction)
        
        let cancelAction = UIPreviewAction(title: "Cancel", style: .destructive) { (action: UIPreviewAction, vc: UIViewController) -> Void in
            self.dismiss(animated: true, completion: nil)
        }
        
        actions.append(cancelAction)
        
        return actions
    }
    
    func configureForMovieId(_movieId: String, _imageUrl: String){
        self.movieId = _movieId
        self.movieImageUrl = _imageUrl
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.isModal() && self.navigationController != nil {
            self.navigationItem.setLeftBarButton(UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(FullPageMovieViewController.closeButtonPressed(_:))), animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getMovieSummary()
        self.getCast()
        
        self.watchListButton.titleLabel?.textAlignment = .center
    }
    
    fileprivate func getMovieSummary(){
        
        self.activityIndicator.startAnimating()
        
        TraktManager.sharedManager.getMovieSummary(movieID: self.movieId, extended: [ExtendedType.Full]){ (result) in
            DispatchQueue.main.async {
                
                self.activityIndicator.stopAnimating()
                
                switch result {
                case .success(let result):
                    self.setupMovie(_movie: result)
                    break
                case .error( _):
                    return
                }
            }
        }
    }
    
    fileprivate func setupMovie(_movie: TraktMovie){
        self.activityIndicator.stopAnimating()
        
        self.fullPageMovie = _movie
        self.fullPageMovie.imageUrl = self.movieImageUrl
        
        self.posterImageView.layer.masksToBounds = false
        self.posterImageView.clipsToBounds = false
        self.posterImageView.layer.shadowColor = UIColor.white.cgColor
        self.posterImageView.layer.shadowOpacity = 0.5
        self.posterImageView.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.posterImageView.layer.shadowRadius = 5
        
        if (self.fullPageMovie.imageUrl != nil){
            self.posterImageView.sd_setImage(with: NSURL(string: self.fullPageMovie.imageUrl!) as URL?, placeholderImage: UIImage(named:"Placeholder"))
        }
        else {
            self.posterImageView.image = UIImage(named:"Placeholder")
        }
        
        self.titleLabel.text = _movie.title
        self.overviewLabel.text = _movie.overview
        self.ratingLabel.text = NSString(format: "Rating: %.1f/10", _movie.rating!) as String
        self.runtimeLabel.text = NSString(format: "Runtime: %d mins", _movie.runtime!) as String
        
        if (_movie.genres != nil) {
            self.genreLabel.text = NSString(format: "Genre: %@", _movie.genreString) as String
        }
        
        if _movie.certification != nil {
            self.certificationLabel.text = NSString(format: "Certification: %d", _movie.certification!) as String
        }
        self.yearLabel.text = NSString(format: "Year: %d", _movie.year!) as String
        
        if (_movie.trailer?.absoluteString.count > 0){
            self.trailerButton.isEnabled = true
            self.trailerButton.isHidden = false
        }

        self.watchListButton.isEnabled = true
        self.watchListButton.isHidden = false
        self.viewSimilarButton.isHidden = false
        
        self.setWatchButtonState(WatchListDataController.sharedInstance.isMovieItemOnWatchList(self.movieId))
        
        self.fullPageMovie = _movie

        self.title = self.fullPageMovie.title
    }
    
    fileprivate func getCast(){
        TraktManager.sharedManager.getPeopleInMovie(movieID: self.movieId, extended: [ExtendedType.Full]){ (result) in
        
            self.activityIndicator.stopAnimating()
            
            switch result {
            case .success(let result):
                self.setupCast(cast: result.cast, crew: result.crew)
                break
            case .error( _):
                return
            }
        }
    }
    
    fileprivate func setupCast(cast: [CastMember], crew: [CrewMember]){
        var i = 0
        
        for castMember in cast {
            
            let character = castMember.character
            let actor = castMember.person?.name
            
            let castObject = CastModel(actor: actor, character: character, movieId: self.movieId)
            
            self.castArray.append(castObject)
            
            if (i > 10){
                break
            }
            
            i += 1
        }
        
        DispatchQueue.main.async {
            self.castTableViewHeightConstraint.constant = CGFloat(24 * self.castArray.count)
            self.castTableView.reloadData()
        }
    }
    
    fileprivate func setWatchButtonState(_ saved: Bool){
        
        self.movieSavedToWatchList = saved
        var title = "Add to Watch List"
        
        if (saved){
            title = "Remove from Watch List"
        }
        
        self.watchListButton.setTitle(title, for: UIControl.State())
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showSimilarViewControllerSegue"){
            let similarViewController = segue.destination as! ShowSimilarViewController
            similarViewController.fullPageMovie = self.fullPageMovie
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.castArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "CastTableViewCell") as! CastTableViewCell
        
        let castModel = self.castArray[indexPath.row] as CastModel
        
        cell.configureForCast(castModel)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 24
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton){
        self.dismiss(animated: true) {}
    }
    
    @IBAction func viewSimilarPressed(_ sender: UIButton){
        let similarMoviesViewController = self.storyboard?.instantiateViewController(withIdentifier: "MoviesCollectionViewController") as! MoviesCollectionViewController
        similarMoviesViewController.delegate = self
        similarMoviesViewController.requestForMovieType(MovieType.related, searchText: self.fullPageMovie.ids.imdb)

        self.navigationController?.show(similarMoviesViewController, sender: nil)
    }
    
    @IBAction func viewTrailerButtonPressed(_ sender: UIButton!){
        UIApplication.shared.openURL(self.fullPageMovie.trailer!)
    }
    
    @IBAction func saveToWatchList(_ sender: UIButton!){
        
        if (self.movieSavedToWatchList){
            WatchListDataController.sharedInstance.deleteFullPageMovie(self.fullPageMovie)
            self.setWatchButtonState(WatchListDataController.sharedInstance.isMovieItemOnWatchList(self.movieId))
        }
        else{
            WatchListDataController.sharedInstance.saveWatchListItem(self.fullPageMovie, completion: { (error, fullMovie) -> Void in
                self.setWatchButtonState(WatchListDataController.sharedInstance.isMovieItemOnWatchList(self.movieId))
                if (error == nil){
                    
                    WatchListDataController.sharedInstance.saveCast(self.castArray, fullMovie: fullMovie!, completion: {(error) -> Void in
                        if (error == nil){
                            
                        }
                    })
                }
           })
        }
    }
    
    func moviesCollectionViewControllerDidSelectItem(_ movie: TraktMovie, controller: MoviesCollectionViewController){
        
        let fullPageMovieViewController = self.storyboard?.instantiateViewController(withIdentifier: "FullPageMovieViewController") as! FullPageMovieViewController;
        fullPageMovieViewController.configureForMovieId(_movieId: movie.ids.imdb!, _imageUrl: movie.imageUrl!)
        self.navigationController?.pushViewController(fullPageMovieViewController, animated: true)
    }
    
}
