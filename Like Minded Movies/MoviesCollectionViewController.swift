//
//  MoviesCollectionViewController.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 14/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit

protocol MoviesCollectionViewControllerDelegate: class {
    func moviesCollectionViewControllerDidSelectItem(_ movie: TraktMovie, controller: MoviesCollectionViewController)
}

enum MovieType {
    case popular
    case trending
    case anticipated
    case search
    case related
}

class MoviesCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIViewControllerPreviewingDelegate {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var titleLabel: UILabel!
    
    weak var delegate: MoviesCollectionViewControllerDelegate?
    var dataSource = Array<TraktMovie>()
    var currentPage: NSInteger = 0
    var selectedMovieType: MovieType = .popular
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        registerForPreviewing(with: self, sourceView: view)
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController?{
        
        if let indexPath = collectionView?.indexPathForItem(at: location) {
        
            if let cell = collectionView?.cellForItem(at: indexPath) {
                let detailVC = storyboard!.instantiateViewController(withIdentifier: "FullPageMovieViewController") as! FullPageMovieViewController
                
                let selectedMovie = self.dataSource[indexPath.row]
                detailVC.movieId = selectedMovie.ids.imdb
                detailVC.movieImageUrl = selectedMovie.imageUrl
                
                detailVC.preferredContentSize = CGSize(width: 0.0, height: 500)
                
                previewingContext.sourceRect = cell.frame
                
                return detailVC
            }
        }
        
        return nil
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController){
        self.navigationController?.show(viewControllerToCommit, sender: nil)
    }
    
    func requestForMovieType(_ movieType: MovieType, searchText: String?){
        
        self.selectedMovieType = movieType
        
        switch (movieType){
            
        case .popular:
            self.getPopularMovies()
            break
            
        case .trending:
            self.getTrendingMovies()
            break
            
        case .anticipated:
            self.getMostAnticipatedMovies()
            break
            
        case .search:
            if ((searchText) != nil){
                self.searchForMovie(searchText!)
            }
            break
            
        case .related:
            if ((searchText) != nil){
                self.getRelatedMoviesForMovie(searchText!)
            }
            break
        }
    }
    
    func resetCollectionView(){
        self.dataSource.removeAll()
        self.collectionView.reloadData()
        self.titleLabel.text = nil
    }
    
    func indexOfTraktMovieForId(_id: String) -> NSInteger?{
        for _movie in self.dataSource{
            if _movie.ids.imdb == _id{
                return self.dataSource.firstIndex(of: _movie)
            }
        }
        
        return nil
    }
    
//    func getOmdbImageUrlForId(_imdbId: String){
//        let url = URL(string: "http://img.omdbapi.com/?i=" + _imdbId + "&h=800&apikey=e266b7ee")
//
//        URLSession.shared.dataTask(with:url!) { (data, response, error) in
//            if error != nil {
//                print(error!)
//            } else {
//                do {
//                    let parsedData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
//
//                    if let val = parsedData["imdbID"] {
//                        self.dataSource[self.indexOfTraktMovieForId(_id: val as! String)!].imageUrl = parsedData["Poster"] as! String?
//                    }
//
//                    DispatchQueue.main.async {
//                        self.collectionView.reloadData()
//                    }
//
//                } catch let error as NSError {
//                    print(error)
//                }
//            }
//
//            }.resume()
//    }
    
    // MARK: Popular
    
    fileprivate func getPopularMovies(){
        
        if (currentPage <= 15){
            
            currentPage += 1
            
            TraktManager.sharedManager.getPopularMovies(page: currentPage, limit: 20, extended: [ExtendedType.Full], completion: { (result) in
                switch result {
                case .success(let result):
                    self.getPopularMoviesCompletionHandler(_movies: result)
                    break
                case .error( _):
                    return
                }
                
            })
        }
    }
    
    fileprivate func getPopularMoviesCompletionHandler(_movies: [TraktMovie]){
        if (self.dataSource.count > 0){
            self.dataSource.removeLast()
        }
        
        for movie in _movies{
            self.dataSource.append(movie)
        }
        
        if (self.currentPage <= 15){
            let movie = TraktMovie()
            movie.title = "Loading..."
            self.dataSource.append(movie)
        }
        
        DispatchQueue.main.async {
            self.titleLabel.text = "Popular:"
            self.collectionView.reloadData()
        }
    }
    
    // MARK: Trending
    
    fileprivate func getTrendingMovies(){
        
        if (currentPage <= 15){
            
            currentPage += 1
            
            TraktManager.sharedManager.getTrendingMovies(page: currentPage, limit: 20, extended: [ExtendedType.Full], completion: { (result) in
                
                switch result {
                case .success(let result):
                    self.getTrendingMoviesCompletionHandler(_movies: result)
                    break
                case .error( _):
                    return
                }
            })
        }
    }
    
    fileprivate func getTrendingMoviesCompletionHandler(_movies: [TraktTrendingMovie]){
        if (self.dataSource.count > 0){
            self.dataSource.removeLast()
        }
        
        for movie in _movies{
            self.dataSource.append(movie.movie)
        }
        
        if (self.currentPage <= 15){
            let movie = TraktMovie()
            movie.title = "Loading..."
            self.dataSource.append(movie)
        }
        
        DispatchQueue.main.async {
            self.titleLabel.text = "Trending:"
            self.collectionView.reloadData()
        }
    }
    
    // MARK: Anticipated
    
    fileprivate func getMostAnticipatedMovies(){
        
        if (currentPage <= 15){
            
            currentPage += 1
            
            TraktManager.sharedManager.getAnticipatedMovies(page: currentPage, limit: 20, completion: { (result) in
                switch result {
                case .success(let result):
                    self.getMostAnticipatedMoviesCompletionHandler(_movies: result)
                    break
                case .error( _):
                    return
                }
            })
        }
    }
    
    fileprivate func getMostAnticipatedMoviesCompletionHandler(_movies: [TraktAnticipatedMovie]){
        if (self.dataSource.count > 0){
            self.dataSource.removeLast()
        }
        
        for movie in _movies{
            self.dataSource.append(movie.movie)
        }
        
        if (self.currentPage <= 15){
            let movie = TraktMovie()
            movie.title = "Loading..."
            self.dataSource.append(movie)
        }
        
        DispatchQueue.main.async {
            self.titleLabel.text = "Most anticipated:"
            self.collectionView.reloadData()
        }
    
    }
    
    // MARK: Search
    
    fileprivate func searchForMovie(_ searchText: String){
        
        TraktManager.sharedManager.search(query: searchText, types: [SearchType.movie], completion:
            { (result) in
                switch result {
                case .success(let result):
                    self.searchForMoviesCompletionHandler(_movies: result)
                    break
                case .error( _):
                    return
                }
        })
    }
    
    fileprivate func searchForMoviesCompletionHandler(_movies: [TraktSearchResult]){
        self.dataSource.removeAll()
        
        for movie in _movies{
            self.dataSource.append(movie.movie!)
        }
        
        if (self.currentPage <= 15){
            let movie = TraktMovie()
            movie.title = "Loading..."
            self.dataSource.append(movie)
        }
        
        DispatchQueue.main.async {
            self.titleLabel.text = "Select the correct film:"
            self.collectionView.reloadData()
        }
        
    }
    
    // MARK: Related
    
    fileprivate func getRelatedMoviesForMovie(_ movieId: String){
        
        TraktManager.sharedManager.getRelatedMovies(movieID: movieId, extended: [ExtendedType.Full], completion: { (result) in
            switch result {
            case .success(let result):
                self.getRelatedMoviesCompletionHandler(_movies: result)
                break
            case .error( _):
                return
            }
        })
    }
    
    fileprivate func getRelatedMoviesCompletionHandler(_movies: [TraktMovie]){
        self.dataSource.removeAll()
        
        for movie in _movies{
            self.dataSource.append(movie)
        }
        
        DispatchQueue.main.async {
            self.titleLabel.text = "You may also like:"
            self.collectionView.reloadData()
        }
    }
    
    // MARK: Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell: MovieCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCollectionViewCell", for: indexPath) as! MovieCollectionViewCell
        
        let movie: TraktMovie = self.dataSource[indexPath.row] as TraktMovie
        
        if(indexPath.row == (self.dataSource.count - 1)){
            self.requestForMovieType(self.selectedMovieType, searchText: nil)
        }
        
        cell.configureForMovie(movie)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let selectedMovie = self.dataSource[indexPath.row]
        
        self.delegate?.moviesCollectionViewControllerDidSelectItem(selectedMovie, controller: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize{
        return CGSize(width: 120, height: 200)
    }
    
}
