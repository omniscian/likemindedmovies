//
//  CastTableViewCell.swift
//  Like Minded Movies
//
//  Created by Ian Layland-Houghton on 22/03/2016.
//  Copyright © 2016 Yak Apps. All rights reserved.
//

import UIKit

class CastTableViewCell: UITableViewCell {

    @IBOutlet var actorLabel : UILabel!
    @IBOutlet var characterLabel : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureForCast(_ castModel: CastModel){
        self.actorLabel.text = castModel.actor
        self.characterLabel.text = castModel.character
    }
    
    override func prepareForReuse() {
        self.actorLabel.text = nil
        self.characterLabel.text = nil
    }

}
